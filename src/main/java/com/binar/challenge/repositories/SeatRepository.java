package com.binar.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.binar.challenge.models.Seat;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Integer> {

}
