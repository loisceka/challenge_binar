package com.binar.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.binar.challenge.models.Booking;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {

}
