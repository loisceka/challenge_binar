package com.binar.challenge.repositories;

import com.binar.challenge.models.Film;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends JpaRepository<Film, Integer> {

	List<Film> findByAired(boolean aired);
	
	Film findByName(String name);
}
