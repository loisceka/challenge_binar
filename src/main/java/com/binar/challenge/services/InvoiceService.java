package com.binar.challenge.services;

import java.awt.Color;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.binar.challenge.models.Booking;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

public class InvoiceService {
	private Booking booking;

	public InvoiceService(Booking booking) {
		this.booking = booking;
	}

	public void generateInvoice(HttpServletResponse response) throws DocumentException, IOException {
		Document document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, response.getOutputStream());

		document.open();
		Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font.setSize(18);
		font.setColor(Color.BLUE);
		Font font2 = FontFactory.getFont(FontFactory.TIMES);
		font2.setSize(14);
		font2.setColor(Color.BLACK);

		Paragraph title = new Paragraph("Detail Movie Invoice", font);
		title.setAlignment(Paragraph.ALIGN_CENTER);
		Paragraph name = new Paragraph("Nama : " + booking.getUserId().getName());
		Paragraph studio = new Paragraph("Studio : " + booking.getSeatId().getStudioName());
		Paragraph seatNumber = new Paragraph("No. Kursi : " + booking.getSeatId().getSeatNumber());
		Paragraph filmName = new Paragraph("Nama Film : " + booking.getScheduleId().getFilmId().getName());
		Paragraph schedule = new Paragraph("Jadwal : " + booking.getScheduleId().getDateAired());
		Paragraph startTime = new Paragraph("Start Time : " + booking.getScheduleId().getStartTime());
		Paragraph endTime = new Paragraph("End Time : " + booking.getScheduleId().getEndTime());

		document.add(title);
		document.add(name);
		document.add(studio);
		document.add(seatNumber);
		document.add(filmName);
		document.add(schedule);
		document.add(startTime);
		document.add(endTime);

		document.close();

	}
}
