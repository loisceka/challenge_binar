package com.binar.challenge.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binar.challenge.models.Booking;
import com.binar.challenge.services.BookingService;

@RestController
@RequestMapping("/booking")
public class BookingController {

	@Autowired
	private BookingService bookingService;

	@GetMapping
	public ResponseEntity<List<Booking>> getAll() {
		return new ResponseEntity<>(bookingService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Booking> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(bookingService.getById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Booking> create(@RequestBody Booking booking) {
		return new ResponseEntity<>(bookingService.create(booking), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Booking> update(@PathVariable("id") Integer id, @RequestBody Booking booking) {
		return new ResponseEntity<>(bookingService.update(id, booking), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Booking> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(bookingService.delete(id), HttpStatus.OK);
	}
}
