package com.binar.challenge.controllers;

import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binar.challenge.models.Booking;
import com.binar.challenge.services.BookingService;
import com.binar.challenge.services.InvoiceService;
import com.lowagie.text.DocumentException;

@RestController
@RequestMapping("/invoice")
public class InvoiceController {
	@Autowired
	private BookingService bookingService;

	@GetMapping("/{id}/export")
	public void exportToPDF(@PathVariable Integer id, HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=MOVIE_INVOICE" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        Booking booking = bookingService.getById(id);
        InvoiceService invoice = new InvoiceService(booking);
        invoice.generateInvoice(response);
    }
}
