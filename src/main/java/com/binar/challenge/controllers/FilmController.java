package com.binar.challenge.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binar.challenge.models.Film;
import com.binar.challenge.services.FilmService;

@RestController
@RequestMapping("/film")
public class FilmController {

	@Autowired
	private FilmService filmService;

	@GetMapping
	public ResponseEntity<List<Film>> getAll() {
		return new ResponseEntity<>(filmService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Film> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(filmService.getById(id), HttpStatus.OK);
	}

	@GetMapping("/aired")
	public ResponseEntity<List<Film>> getAllAired() {
		return new ResponseEntity<>(filmService.getByAired(), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Film> create(@RequestBody Film film) {
		return new ResponseEntity<>(filmService.create(film), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Film> update(@PathVariable("id") Integer id, @RequestBody Film film) {
		return new ResponseEntity<>(filmService.update(id, film), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Film> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(filmService.delete(id), HttpStatus.OK);
	}
}
