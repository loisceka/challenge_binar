package com.binar.challenge.models;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Booking")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Booking {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "booking_id")
	private Integer id;

	@Column(name = "timestamp")
	private Date timestamp;

	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private User userId;

	@JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Schedule scheduleId;

	@JoinColumn(name = "seat_id", referencedColumnName = "seat_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Seat seatId;

	public Booking(Date timestamp, User userId, Schedule scheduleId, Seat seatId) {
		super();
		this.timestamp = timestamp;
		this.userId = userId;
		this.scheduleId = scheduleId;
		this.seatId = seatId;
	}
}
